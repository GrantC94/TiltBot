// Importing mocha and chai
const mocha = require('mocha')
const mocha_sinon = require('mocha-sinon')
const chai = require('chai')

const nock = require('nock')

const tiltBot = require('../bots/tiltBot.js')
const botUtils = require('../bots/botUtils.js');

const expect = chai.expect

const testLeagueAccountInfo = []
testLeagueAccountInfo['Brain%20Too%20Smooth'] = {
        'id': 'Tvi-q2XyShXhC93aW_CQb3_JdzGQdYtwpRMJEUk2wUKrj-0',
        'accountId': 'vpEn5OaQdzkqU5OxxH97Mkecss4c2Hxiw3utzBY_ogM-OSQ',
        'puuid': 'CWaUiFQRw7WU_iQA4FQaC6efrs94Z96QmuGCAXxqQHk8gcfr8VCbO51-1FkeBcKs7EvYMegvmbRQ0A',
        'name': 'Brain Too Smooth',
        'profileIconId': 1627,
        'revisionDate': 1635217445000,
        'summonerLevel': 159
    }

// Group of tests using describe
describe('TiltBot: ', function () {
    
    beforeEach(function() {
        this.sinon.stub(botUtils, 'getLeagueAccountInfo').returns(testLeagueAccountInfo)
    });
    
    it('can be disabled', () => {
        this.sinon.stub(console, 'log');
        tiltBot.initialize(false);
        expect( console.log.calledOnce ).to.be.true;
        expect( console.log.calledWith('Tiltbot Disabled.') ).to.be.true;
    })
    
    it.only('runs', () => {
        nock('https://api.groupme.com')
        .post('/v3/bots/post')
        .reply(200, '["NA1_4087360956"]')
        
        nock('https://americas.api.riotgames.com')
        .get(/^\/lol\/match\/v5\/matches\/by-puuid\/.*\/ids\?queue=.*&count=1&start=0&api_key=.*/)
        .times(3)
        .reply(200, '["NA1_4087360957"]')
        tiltBot.initialize(true);
        
        nock('https://americas.api.riotgames.com')
        .get(/^\/lol\/match\/v5\/matches\/by-puuid\/.*\/ids\?queue=.*&count=1&start=0&api_key=.*/)
        .times(3)
        .reply(200, '["NA1_4087360956"]')
        
        nock('https://americas.api.riotgames.com')
        .get(/^\/lol\/match\/v5\/matches\/.*\?api_key=.*/)
        .times(3)
        .reply(200, {
    "metadata": {
        "dataVersion": "2",
        "matchId": "NA1_4087360957",
        "participants": [
            "CWaUiFQRw7WU_iQA4FQaC6efrs94Z96QmuGCAXxqQHk8gcfr8VCbO51-1FkeBcKs7EvYMegvmbRQ0A",
            "uXqwkzR6TdDSrUQsUw_0tJx0q5NHTbHRgj3Lt1mbnBw1CYMQNEMuS066yUdHf6N6olooXc3kTca4pQ",
            "mn2HCiVXUeLWcJGUjMcLZwtR9doKNGgicN2G4-BvkASE6V0FlFjRtyZItnex2slEpZsHKHLlLHV_eA",
            "gtUU84RZb8AE6FP0VmIDhtRxPP51xi1-cS69Xvv0AEHY0baqJfb_M06pHwowy0RqQhOJ5PljLAEbeA",
            "Dfsol3WqOKPLMWVJqc2vrJ-N-ZP4PYiZl4-j0XsLLE7JO_Nnaq6hgi2Y1-8mapDwVbM6qO5hGAVk_w",
            "Mm_89UkZzo9yPYGp0zLtqIpLx5KaDzIim5r3er2SDrfvqP7TCYFPVq82X_uWsSvMX16yaZJ06Ha-IA",
            "B8Sil4R738tpoObqJUtZPITiLToQZNAZqbaLf6F37rwjAwJXf65l4qRhAVgD3vNwdUBm977UPciJ7Q",
            "_ES7JGtYOHmERubPlHcHmgH0qk77AEpeg6IVlvHqxSL5uqyq885dujtDOl_-SMOHjdP78wfTa2msfw",
            "Ulsl-a6cg7kMpJEVQ4Pz9s22-7GEsbbFnsIULLbSzNbQLwncP_dGGJTGb42_LymUhr4YXFIZkS8EJA",
            "s3SXI23BnHU98aeyJil9_-tPk-NIB9Kojs981sTFOQC2udkc31jVOpGmPtoF7dD2_TXSrvdfXhj0XA"
        ]
    },
    "info": {
        "gameCreation": 1635701523000,
        "gameDuration": 2077,
        "gameEndTimestamp": 1635703635254,
        "gameId": 4087360957,
        "gameMode": "CLASSIC",
        "gameName": "teambuilder-match-4087360957",
        "gameStartTimestamp": 1635701557790,
        "gameType": "MATCHED_GAME",
        "gameVersion": "11.21.403.3002",
        "mapId": 11,
        "participants": [
            {
                "assists": 7,
                "baronKills": 0,
                "bountyLevel": 4,
                "champExperience": 15925,
                "champLevel": 16,
                "championId": 223,
                "championName": "TahmKench",
                "championTransform": 0,
                "consumablesPurchased": 3,
                "damageDealtToBuildings": 3625,
                "damageDealtToObjectives": 7960,
                "damageDealtToTurrets": 3625,
                "damageSelfMitigated": 55150,
                "deaths": 3,
                "detectorWardsPlaced": 0,
                "doubleKills": 0,
                "dragonKills": 0,
                "firstBloodAssist": false,
                "firstBloodKill": false,
                "firstTowerAssist": false,
                "firstTowerKill": false,
                "gameEndedInEarlySurrender": false,
                "gameEndedInSurrender": true,
                "goldEarned": 11725,
                "goldSpent": 10050,
                "individualPosition": "TOP",
                "inhibitorKills": 1,
                "inhibitorTakedowns": 1,
                "inhibitorsLost": 0,
                "item0": 6662,
                "item1": 3047,
                "item2": 8001,
                "item3": 3075,
                "item4": 3067,
                "item5": 0,
                "item6": 3340,
                "itemsPurchased": 21,
                "killingSprees": 1,
                "kills": 7,
                "lane": "TOP",
                "largestCriticalStrike": 2,
                "largestKillingSpree": 4,
                "largestMultiKill": 1,
                "longestTimeSpentLiving": 546,
                "magicDamageDealt": 83457,
                "magicDamageDealtToChampions": 24373,
                "magicDamageTaken": 27303,
                "neutralMinionsKilled": 8,
                "nexusKills": 0,
                "nexusLost": 0,
                "nexusTakedowns": 0,
                "objectivesStolen": 0,
                "objectivesStolenAssists": 0,
                "participantId": 1,
                "pentaKills": 0,
                "perks": {
                    "statPerks": {
                        "defense": 5001,
                        "flex": 5002,
                        "offense": 5005
                    },
                    "styles": [
                        {
                            "description": "primaryStyle",
                            "selections": [
                                {
                                    "perk": 8437,
                                    "var1": 2136,
                                    "var2": 1566,
                                    "var3": 0
                                },
                                {
                                    "perk": 8446,
                                    "var1": 2053,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8429,
                                    "var1": 65,
                                    "var2": 20,
                                    "var3": 13
                                },
                                {
                                    "perk": 8453,
                                    "var1": 2883,
                                    "var2": 2188,
                                    "var3": 0
                                }
                            ],
                            "style": 8400
                        },
                        {
                            "description": "subStyle",
                            "selections": [
                                {
                                    "perk": 9111,
                                    "var1": 2792,
                                    "var2": 280,
                                    "var3": 0
                                },
                                {
                                    "perk": 9104,
                                    "var1": 22,
                                    "var2": 40,
                                    "var3": 0
                                }
                            ],
                            "style": 8000
                        }
                    ]
                },
                "physicalDamageDealt": 27236,
                "physicalDamageDealtToChampions": 4144,
                "physicalDamageTaken": 20462,
                "profileIcon": 1627,
                "puuid": "CWaUiFQRw7WU_iQA4FQaC6efrs94Z96QmuGCAXxqQHk8gcfr8VCbO51-1FkeBcKs7EvYMegvmbRQ0A",
                "quadraKills": 0,
                "riotIdName": "",
                "riotIdTagline": "",
                "role": "SOLO",
                "sightWardsBoughtInGame": 0,
                "spell1Casts": 135,
                "spell2Casts": 34,
                "spell3Casts": 21,
                "spell4Casts": 31,
                "summoner1Casts": 4,
                "summoner1Id": 12,
                "summoner2Casts": 6,
                "summoner2Id": 4,
                "summonerId": "Tvi-q2XyShXhC93aW_CQb3_JdzGQdYtwpRMJEUk2wUKrj-0",
                "summonerLevel": 160,
                "summonerName": "Brain Too Smooth",
                "teamEarlySurrendered": false,
                "teamId": 100,
                "teamPosition": "TOP",
                "timeCCingOthers": 66,
                "timePlayed": 2077,
                "totalDamageDealt": 126429,
                "totalDamageDealtToChampions": 28517,
                "totalDamageShieldedOnTeammates": 0,
                "totalDamageTaken": 49253,
                "totalHeal": 24580,
                "totalHealsOnTeammates": 0,
                "totalMinionsKilled": 140,
                "totalTimeCCDealt": 1018,
                "totalTimeSpentDead": 95,
                "totalUnitsHealed": 1,
                "tripleKills": 0,
                "trueDamageDealt": 15735,
                "trueDamageDealtToChampions": 0,
                "trueDamageTaken": 1487,
                "turretKills": 2,
                "turretTakedowns": 3,
                "turretsLost": 3,
                "unrealKills": 0,
                "visionScore": 18,
                "visionWardsBoughtInGame": 0,
                "wardsKilled": 0,
                "wardsPlaced": 12,
                "win": false
            },
            {
                "assists": 9,
                "baronKills": 0,
                "bountyLevel": 2,
                "champExperience": 16444,
                "champLevel": 16,
                "championId": 876,
                "championName": "Lillia",
                "championTransform": 0,
                "consumablesPurchased": 2,
                "damageDealtToBuildings": 2679,
                "damageDealtToObjectives": 18659,
                "damageDealtToTurrets": 2679,
                "damageSelfMitigated": 20889,
                "deaths": 3,
                "detectorWardsPlaced": 1,
                "doubleKills": 0,
                "dragonKills": 1,
                "firstBloodAssist": false,
                "firstBloodKill": false,
                "firstTowerAssist": false,
                "firstTowerKill": false,
                "gameEndedInEarlySurrender": false,
                "gameEndedInSurrender": true,
                "goldEarned": 15240,
                "goldSpent": 14500,
                "individualPosition": "JUNGLE",
                "inhibitorKills": 0,
                "inhibitorTakedowns": 0,
                "inhibitorsLost": 0,
                "item0": 3157,
                "item1": 3165,
                "item2": 4637,
                "item3": 6653,
                "item4": 3158,
                "item5": 1058,
                "item6": 3364,
                "itemsPurchased": 22,
                "killingSprees": 2,
                "kills": 10,
                "lane": "JUNGLE",
                "largestCriticalStrike": 0,
                "largestKillingSpree": 7,
                "largestMultiKill": 1,
                "longestTimeSpentLiving": 1478,
                "magicDamageDealt": 129175,
                "magicDamageDealtToChampions": 15762,
                "magicDamageTaken": 11672,
                "neutralMinionsKilled": 138,
                "nexusKills": 0,
                "nexusLost": 0,
                "nexusTakedowns": 0,
                "objectivesStolen": 0,
                "objectivesStolenAssists": 0,
                "participantId": 2,
                "pentaKills": 0,
                "perks": {
                    "statPerks": {
                        "defense": 5002,
                        "flex": 5008,
                        "offense": 5008
                    },
                    "styles": [
                        {
                            "description": "primaryStyle",
                            "selections": [
                                {
                                    "perk": 8128,
                                    "var1": 1489,
                                    "var2": 19,
                                    "var3": 0
                                },
                                {
                                    "perk": 8126,
                                    "var1": 830,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8138,
                                    "var1": 30,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8135,
                                    "var1": 3508,
                                    "var2": 5,
                                    "var3": 0
                                }
                            ],
                            "style": 8100
                        },
                        {
                            "description": "subStyle",
                            "selections": [
                                {
                                    "perk": 8234,
                                    "var1": 16699,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8232,
                                    "var1": 4,
                                    "var2": 40,
                                    "var3": 0
                                }
                            ],
                            "style": 8200
                        }
                    ]
                },
                "physicalDamageDealt": 18331,
                "physicalDamageDealtToChampions": 881,
                "physicalDamageTaken": 20567,
                "profileIcon": 1151,
                "puuid": "uXqwkzR6TdDSrUQsUw_0tJx0q5NHTbHRgj3Lt1mbnBw1CYMQNEMuS066yUdHf6N6olooXc3kTca4pQ",
                "quadraKills": 0,
                "riotIdName": "",
                "riotIdTagline": "",
                "role": "NONE",
                "sightWardsBoughtInGame": 0,
                "spell1Casts": 165,
                "spell2Casts": 54,
                "spell3Casts": 47,
                "spell4Casts": 8,
                "summoner1Casts": 15,
                "summoner1Id": 11,
                "summoner2Casts": 5,
                "summoner2Id": 4,
                "summonerId": "30sIPxK_gVv3m6I3r_cKyS7IOCI0sjgVGI828JCTg1kbx4g",
                "summonerLevel": 302,
                "summonerName": "uFake",
                "teamEarlySurrendered": false,
                "teamId": 100,
                "teamPosition": "JUNGLE",
                "timeCCingOthers": 21,
                "timePlayed": 2077,
                "totalDamageDealt": 196272,
                "totalDamageDealtToChampions": 21643,
                "totalDamageShieldedOnTeammates": 0,
                "totalDamageTaken": 32961,
                "totalHeal": 13627,
                "totalHealsOnTeammates": 0,
                "totalMinionsKilled": 77,
                "totalTimeCCDealt": 276,
                "totalTimeSpentDead": 143,
                "totalUnitsHealed": 1,
                "tripleKills": 0,
                "trueDamageDealt": 48766,
                "trueDamageDealtToChampions": 5000,
                "trueDamageTaken": 721,
                "turretKills": 0,
                "turretTakedowns": 4,
                "turretsLost": 3,
                "unrealKills": 0,
                "visionScore": 23,
                "visionWardsBoughtInGame": 2,
                "wardsKilled": 3,
                "wardsPlaced": 2,
                "win": true
            },
            {
                "assists": 9,
                "baronKills": 0,
                "bountyLevel": 2,
                "champExperience": 17667,
                "champLevel": 17,
                "championId": 711,
                "championName": "Vex",
                "championTransform": 0,
                "consumablesPurchased": 0,
                "damageDealtToBuildings": 3936,
                "damageDealtToObjectives": 6652,
                "damageDealtToTurrets": 3936,
                "damageSelfMitigated": 11749,
                "deaths": 5,
                "detectorWardsPlaced": 0,
                "doubleKills": 2,
                "dragonKills": 0,
                "firstBloodAssist": false,
                "firstBloodKill": false,
                "firstTowerAssist": false,
                "firstTowerKill": false,
                "gameEndedInEarlySurrender": false,
                "gameEndedInSurrender": true,
                "goldEarned": 14318,
                "goldSpent": 13750,
                "individualPosition": "MIDDLE",
                "inhibitorKills": 0,
                "inhibitorTakedowns": 1,
                "inhibitorsLost": 0,
                "item0": 6655,
                "item1": 4628,
                "item2": 3157,
                "item3": 3089,
                "item4": 3020,
                "item5": 0,
                "item6": 3340,
                "itemsPurchased": 15,
                "killingSprees": 5,
                "kills": 14,
                "lane": "MIDDLE",
                "largestCriticalStrike": 0,
                "largestKillingSpree": 5,
                "largestMultiKill": 2,
                "longestTimeSpentLiving": 673,
                "magicDamageDealt": 110641,
                "magicDamageDealtToChampions": 29360,
                "magicDamageTaken": 11882,
                "neutralMinionsKilled": 0,
                "nexusKills": 0,
                "nexusLost": 0,
                "nexusTakedowns": 0,
                "objectivesStolen": 0,
                "objectivesStolenAssists": 0,
                "participantId": 3,
                "pentaKills": 0,
                "perks": {
                    "statPerks": {
                        "defense": 5003,
                        "flex": 5008,
                        "offense": 5008
                    },
                    "styles": [
                        {
                            "description": "primaryStyle",
                            "selections": [
                                {
                                    "perk": 8112,
                                    "var1": 1517,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8126,
                                    "var1": 485,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8138,
                                    "var1": 30,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8106,
                                    "var1": 5,
                                    "var2": 0,
                                    "var3": 0
                                }
                            ],
                            "style": 8100
                        },
                        {
                            "description": "subStyle",
                            "selections": [
                                {
                                    "perk": 8345,
                                    "var1": 3,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8313,
                                    "var1": 0,
                                    "var2": 0,
                                    "var3": 0
                                }
                            ],
                            "style": 8300
                        }
                    ]
                },
                "physicalDamageDealt": 12434,
                "physicalDamageDealtToChampions": 904,
                "physicalDamageTaken": 6075,
                "profileIcon": 4270,
                "puuid": "mn2HCiVXUeLWcJGUjMcLZwtR9doKNGgicN2G4-BvkASE6V0FlFjRtyZItnex2slEpZsHKHLlLHV_eA",
                "quadraKills": 0,
                "riotIdName": "",
                "riotIdTagline": "",
                "role": "SOLO",
                "sightWardsBoughtInGame": 0,
                "spell1Casts": 120,
                "spell2Casts": 43,
                "spell3Casts": 52,
                "spell4Casts": 30,
                "summoner1Casts": 4,
                "summoner1Id": 14,
                "summoner2Casts": 4,
                "summoner2Id": 4,
                "summonerId": "MjXYpBOOIoiz0QmQPaNfN6md4Q-I57lq5U1WV3_DrVoL4IU",
                "summonerLevel": 281,
                "summonerName": "Dramonic",
                "teamEarlySurrendered": false,
                "teamId": 100,
                "teamPosition": "MIDDLE",
                "timeCCingOthers": 26,
                "timePlayed": 2077,
                "totalDamageDealt": 127127,
                "totalDamageDealtToChampions": 31878,
                "totalDamageShieldedOnTeammates": 0,
                "totalDamageTaken": 18092,
                "totalHeal": 695,
                "totalHealsOnTeammates": 0,
                "totalMinionsKilled": 162,
                "totalTimeCCDealt": 373,
                "totalTimeSpentDead": 212,
                "totalUnitsHealed": 1,
                "tripleKills": 0,
                "trueDamageDealt": 4051,
                "trueDamageDealtToChampions": 1613,
                "trueDamageTaken": 134,
                "turretKills": 3,
                "turretTakedowns": 4,
                "turretsLost": 3,
                "unrealKills": 0,
                "visionScore": 15,
                "visionWardsBoughtInGame": 0,
                "wardsKilled": 3,
                "wardsPlaced": 7,
                "win": true
            },
            {
                "assists": 9,
                "baronKills": 0,
                "bountyLevel": 6,
                "champExperience": 15570,
                "champLevel": 16,
                "championId": 21,
                "championName": "MissFortune",
                "championTransform": 0,
                "consumablesPurchased": 3,
                "damageDealtToBuildings": 12643,
                "damageDealtToObjectives": 29203,
                "damageDealtToTurrets": 12643,
                "damageSelfMitigated": 10027,
                "deaths": 4,
                "detectorWardsPlaced": 2,
                "doubleKills": 2,
                "dragonKills": 2,
                "firstBloodAssist": false,
                "firstBloodKill": false,
                "firstTowerAssist": false,
                "firstTowerKill": false,
                "gameEndedInEarlySurrender": false,
                "gameEndedInSurrender": true,
                "goldEarned": 14141,
                "goldSpent": 13550,
                "individualPosition": "BOTTOM",
                "inhibitorKills": 0,
                "inhibitorTakedowns": 1,
                "inhibitorsLost": 0,
                "item0": 3814,
                "item1": 3142,
                "item2": 6692,
                "item3": 3009,
                "item4": 6676,
                "item5": 0,
                "item6": 3363,
                "itemsPurchased": 20,
                "killingSprees": 2,
                "kills": 9,
                "lane": "BOTTOM",
                "largestCriticalStrike": 833,
                "largestKillingSpree": 6,
                "largestMultiKill": 3,
                "longestTimeSpentLiving": 525,
                "magicDamageDealt": 15362,
                "magicDamageDealtToChampions": 3051,
                "magicDamageTaken": 4802,
                "neutralMinionsKilled": 44,
                "nexusKills": 0,
                "nexusLost": 0,
                "nexusTakedowns": 0,
                "objectivesStolen": 0,
                "objectivesStolenAssists": 0,
                "participantId": 4,
                "pentaKills": 0,
                "perks": {
                    "statPerks": {
                        "defense": 5003,
                        "flex": 5008,
                        "offense": 5008
                    },
                    "styles": [
                        {
                            "description": "primaryStyle",
                            "selections": [
                                {
                                    "perk": 8229,
                                    "var1": 2872,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8226,
                                    "var1": 250,
                                    "var2": 1406,
                                    "var3": 0
                                },
                                {
                                    "perk": 8233,
                                    "var1": 21,
                                    "var2": 10,
                                    "var3": 0
                                },
                                {
                                    "perk": 8236,
                                    "var1": 28,
                                    "var2": 0,
                                    "var3": 0
                                }
                            ],
                            "style": 8200
                        },
                        {
                            "description": "subStyle",
                            "selections": [
                                {
                                    "perk": 8304,
                                    "var1": 11,
                                    "var2": 1,
                                    "var3": 5
                                },
                                {
                                    "perk": 8345,
                                    "var1": 3,
                                    "var2": 0,
                                    "var3": 0
                                }
                            ],
                            "style": 8300
                        }
                    ]
                },
                "physicalDamageDealt": 190565,
                "physicalDamageDealtToChampions": 29264,
                "physicalDamageTaken": 15522,
                "profileIcon": 4414,
                "puuid": "gtUU84RZb8AE6FP0VmIDhtRxPP51xi1-cS69Xvv0AEHY0baqJfb_M06pHwowy0RqQhOJ5PljLAEbeA",
                "quadraKills": 0,
                "riotIdName": "",
                "riotIdTagline": "",
                "role": "CARRY",
                "sightWardsBoughtInGame": 0,
                "spell1Casts": 106,
                "spell2Casts": 55,
                "spell3Casts": 67,
                "spell4Casts": 12,
                "summoner1Casts": 6,
                "summoner1Id": 4,
                "summoner2Casts": 6,
                "summoner2Id": 7,
                "summonerId": "Ie3ebpKTZoTOKewY9n3BUd9vR2NqtAToMFAbjoeiBLK6",
                "summonerLevel": 453,
                "summonerName": "Sinnlos",
                "teamEarlySurrendered": false,
                "teamId": 100,
                "teamPosition": "BOTTOM",
                "timeCCingOthers": 15,
                "timePlayed": 2077,
                "totalDamageDealt": 223664,
                "totalDamageDealtToChampions": 32573,
                "totalDamageShieldedOnTeammates": 0,
                "totalDamageTaken": 20542,
                "totalHeal": 3122,
                "totalHealsOnTeammates": 1011,
                "totalMinionsKilled": 196,
                "totalTimeCCDealt": 314,
                "totalTimeSpentDead": 112,
                "totalUnitsHealed": 4,
                "tripleKills": 1,
                "trueDamageDealt": 17736,
                "trueDamageDealtToChampions": 258,
                "trueDamageTaken": 218,
                "turretKills": 1,
                "turretTakedowns": 6,
                "turretsLost": 3,
                "unrealKills": 0,
                "visionScore": 45,
                "visionWardsBoughtInGame": 2,
                "wardsKilled": 3,
                "wardsPlaced": 14,
                "win": true
            },
            {
                "assists": 20,
                "baronKills": 0,
                "bountyLevel": 0,
                "champExperience": 12778,
                "champLevel": 14,
                "championId": 117,
                "championName": "Lulu",
                "championTransform": 0,
                "consumablesPurchased": 7,
                "damageDealtToBuildings": 1377,
                "damageDealtToObjectives": 1715,
                "damageDealtToTurrets": 1377,
                "damageSelfMitigated": 9304,
                "deaths": 9,
                "detectorWardsPlaced": 4,
                "doubleKills": 0,
                "dragonKills": 0,
                "firstBloodAssist": false,
                "firstBloodKill": false,
                "firstTowerAssist": false,
                "firstTowerKill": false,
                "gameEndedInEarlySurrender": false,
                "gameEndedInSurrender": true,
                "goldEarned": 8856,
                "goldSpent": 7425,
                "individualPosition": "UTILITY",
                "inhibitorKills": 0,
                "inhibitorTakedowns": 1,
                "inhibitorsLost": 0,
                "item0": 3158,
                "item1": 2065,
                "item2": 3107,
                "item3": 3114,
                "item4": 2055,
                "item5": 3853,
                "item6": 3364,
                "itemsPurchased": 23,
                "killingSprees": 0,
                "kills": 1,
                "lane": "BOTTOM",
                "largestCriticalStrike": 0,
                "largestKillingSpree": 0,
                "largestMultiKill": 1,
                "longestTimeSpentLiving": 499,
                "magicDamageDealt": 13698,
                "magicDamageDealtToChampions": 6335,
                "magicDamageTaken": 10075,
                "neutralMinionsKilled": 0,
                "nexusKills": 0,
                "nexusLost": 0,
                "nexusTakedowns": 0,
                "objectivesStolen": 0,
                "objectivesStolenAssists": 0,
                "participantId": 5,
                "pentaKills": 0,
                "perks": {
                    "statPerks": {
                        "defense": 5002,
                        "flex": 5008,
                        "offense": 5008
                    },
                    "styles": [
                        {
                            "description": "primaryStyle",
                            "selections": [
                                {
                                    "perk": 8214,
                                    "var1": 545,
                                    "var2": 1403,
                                    "var3": 0
                                },
                                {
                                    "perk": 8226,
                                    "var1": 250,
                                    "var2": 580,
                                    "var3": 0
                                },
                                {
                                    "perk": 8210,
                                    "var1": 13,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8237,
                                    "var1": 467,
                                    "var2": 0,
                                    "var3": 0
                                }
                            ],
                            "style": 8200
                        },
                        {
                            "description": "subStyle",
                            "selections": [
                                {
                                    "perk": 8473,
                                    "var1": 1052,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8453,
                                    "var1": 484,
                                    "var2": 1690,
                                    "var3": 0
                                }
                            ],
                            "style": 8400
                        }
                    ]
                },
                "physicalDamageDealt": 3241,
                "physicalDamageDealtToChampions": 880,
                "physicalDamageTaken": 9838,
                "profileIcon": 4793,
                "puuid": "Dfsol3WqOKPLMWVJqc2vrJ-N-ZP4PYiZl4-j0XsLLE7JO_Nnaq6hgi2Y1-8mapDwVbM6qO5hGAVk_w",
                "quadraKills": 0,
                "riotIdName": "",
                "riotIdTagline": "",
                "role": "SUPPORT",
                "sightWardsBoughtInGame": 0,
                "spell1Casts": 49,
                "spell2Casts": 73,
                "spell3Casts": 81,
                "spell4Casts": 7,
                "summoner1Casts": 3,
                "summoner1Id": 3,
                "summoner2Casts": 4,
                "summoner2Id": 4,
                "summonerId": "XH3Gwj01FYJA4HjkXRvmJbd3ZFUlqs8bD_nwapimdLNEAD8",
                "summonerLevel": 190,
                "summonerName": "Zarxes",
                "teamEarlySurrendered": false,
                "teamId": 100,
                "teamPosition": "UTILITY",
                "timeCCingOthers": 33,
                "timePlayed": 2077,
                "totalDamageDealt": 17185,
                "totalDamageDealtToChampions": 7462,
                "totalDamageShieldedOnTeammates": 6667,
                "totalDamageTaken": 20159,
                "totalHeal": 4299,
                "totalHealsOnTeammates": 2233,
                "totalMinionsKilled": 16,
                "totalTimeCCDealt": 208,
                "totalTimeSpentDead": 224,
                "totalUnitsHealed": 6,
                "tripleKills": 0,
                "trueDamageDealt": 245,
                "trueDamageDealtToChampions": 245,
                "trueDamageTaken": 245,
                "turretKills": 2,
                "turretTakedowns": 4,
                "turretsLost": 3,
                "unrealKills": 0,
                "visionScore": 53,
                "visionWardsBoughtInGame": 5,
                "wardsKilled": 4,
                "wardsPlaced": 22,
                "win": true
            },
            {
                "assists": 4,
                "baronKills": 0,
                "bountyLevel": 0,
                "champExperience": 12726,
                "champLevel": 14,
                "championId": 84,
                "championName": "Akali",
                "championTransform": 0,
                "consumablesPurchased": 3,
                "damageDealtToBuildings": 303,
                "damageDealtToObjectives": 303,
                "damageDealtToTurrets": 303,
                "damageSelfMitigated": 16143,
                "deaths": 13,
                "detectorWardsPlaced": 1,
                "doubleKills": 1,
                "dragonKills": 0,
                "firstBloodAssist": false,
                "firstBloodKill": false,
                "firstTowerAssist": false,
                "firstTowerKill": false,
                "gameEndedInEarlySurrender": false,
                "gameEndedInSurrender": true,
                "goldEarned": 10288,
                "goldSpent": 9775,
                "individualPosition": "TOP",
                "inhibitorKills": 0,
                "inhibitorTakedowns": 0,
                "inhibitorsLost": 1,
                "item0": 4637,
                "item1": 3020,
                "item2": 2420,
                "item3": 4633,
                "item4": 3108,
                "item5": 1082,
                "item6": 3364,
                "itemsPurchased": 20,
                "killingSprees": 2,
                "kills": 5,
                "lane": "TOP",
                "largestCriticalStrike": 0,
                "largestKillingSpree": 2,
                "largestMultiKill": 2,
                "longestTimeSpentLiving": 466,
                "magicDamageDealt": 75363,
                "magicDamageDealtToChampions": 20020,
                "magicDamageTaken": 27296,
                "neutralMinionsKilled": 0,
                "nexusKills": 0,
                "nexusLost": 0,
                "nexusTakedowns": 0,
                "objectivesStolen": 0,
                "objectivesStolenAssists": 0,
                "participantId": 6,
                "pentaKills": 0,
                "perks": {
                    "statPerks": {
                        "defense": 5002,
                        "flex": 5008,
                        "offense": 5008
                    },
                    "styles": [
                        {
                            "description": "primaryStyle",
                            "selections": [
                                {
                                    "perk": 8010,
                                    "var1": 582,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8009,
                                    "var1": 675,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 9105,
                                    "var1": 22,
                                    "var2": 10,
                                    "var3": 0
                                },
                                {
                                    "perk": 8014,
                                    "var1": 556,
                                    "var2": 0,
                                    "var3": 0
                                }
                            ],
                            "style": 8000
                        },
                        {
                            "description": "subStyle",
                            "selections": [
                                {
                                    "perk": 8139,
                                    "var1": 1191,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8135,
                                    "var1": 1041,
                                    "var2": 4,
                                    "var3": 0
                                }
                            ],
                            "style": 8100
                        }
                    ]
                },
                "physicalDamageDealt": 13645,
                "physicalDamageDealtToChampions": 2061,
                "physicalDamageTaken": 11552,
                "profileIcon": 4070,
                "puuid": "Mm_89UkZzo9yPYGp0zLtqIpLx5KaDzIim5r3er2SDrfvqP7TCYFPVq82X_uWsSvMX16yaZJ06Ha-IA",
                "quadraKills": 0,
                "riotIdName": "",
                "riotIdTagline": "",
                "role": "SOLO",
                "sightWardsBoughtInGame": 0,
                "spell1Casts": 124,
                "spell2Casts": 28,
                "spell3Casts": 66,
                "spell4Casts": 31,
                "summoner1Casts": 5,
                "summoner1Id": 4,
                "summoner2Casts": 4,
                "summoner2Id": 12,
                "summonerId": "oiOKuv5mx7PQLnYDEcUSjiSlOl2QCLxlamIBhI91sLI-C-k",
                "summonerLevel": 341,
                "summonerName": "Thictor",
                "teamEarlySurrendered": false,
                "teamId": 200,
                "teamPosition": "TOP",
                "timeCCingOthers": 1,
                "timePlayed": 2077,
                "totalDamageDealt": 107730,
                "totalDamageDealtToChampions": 23032,
                "totalDamageShieldedOnTeammates": 0,
                "totalDamageTaken": 41209,
                "totalHeal": 4223,
                "totalHealsOnTeammates": 0,
                "totalMinionsKilled": 150,
                "totalTimeCCDealt": 63,
                "totalTimeSpentDead": 454,
                "totalUnitsHealed": 1,
                "tripleKills": 0,
                "trueDamageDealt": 18722,
                "trueDamageDealtToChampions": 950,
                "trueDamageTaken": 2360,
                "turretKills": 0,
                "turretTakedowns": 1,
                "turretsLost": 8,
                "unrealKills": 0,
                "visionScore": 19,
                "visionWardsBoughtInGame": 1,
                "wardsKilled": 3,
                "wardsPlaced": 2,
                "win": false
            },
            {
                "assists": 3,
                "baronKills": 0,
                "bountyLevel": 0,
                "champExperience": 14426,
                "champLevel": 15,
                "championId": 141,
                "championName": "Kayn",
                "championTransform": 2,
                "consumablesPurchased": 2,
                "damageDealtToBuildings": 0,
                "damageDealtToObjectives": 15988,
                "damageDealtToTurrets": 0,
                "damageSelfMitigated": 17095,
                "deaths": 6,
                "detectorWardsPlaced": 2,
                "doubleKills": 0,
                "dragonKills": 2,
                "firstBloodAssist": false,
                "firstBloodKill": false,
                "firstTowerAssist": false,
                "firstTowerKill": false,
                "gameEndedInEarlySurrender": false,
                "gameEndedInSurrender": true,
                "goldEarned": 10338,
                "goldSpent": 9300,
                "individualPosition": "JUNGLE",
                "inhibitorKills": 0,
                "inhibitorTakedowns": 0,
                "inhibitorsLost": 1,
                "item0": 3142,
                "item1": 6029,
                "item2": 0,
                "item3": 3042,
                "item4": 3044,
                "item5": 3158,
                "item6": 3364,
                "itemsPurchased": 17,
                "killingSprees": 0,
                "kills": 1,
                "lane": "JUNGLE",
                "largestCriticalStrike": 0,
                "largestKillingSpree": 0,
                "largestMultiKill": 1,
                "longestTimeSpentLiving": 1009,
                "magicDamageDealt": 8830,
                "magicDamageDealtToChampions": 809,
                "magicDamageTaken": 15922,
                "neutralMinionsKilled": 189,
                "nexusKills": 0,
                "nexusLost": 0,
                "nexusTakedowns": 0,
                "objectivesStolen": 0,
                "objectivesStolenAssists": 0,
                "participantId": 7,
                "pentaKills": 0,
                "perks": {
                    "statPerks": {
                        "defense": 5002,
                        "flex": 5008,
                        "offense": 5008
                    },
                    "styles": [
                        {
                            "description": "primaryStyle",
                            "selections": [
                                {
                                    "perk": 8128,
                                    "var1": 451,
                                    "var2": 10,
                                    "var3": 0
                                },
                                {
                                    "perk": 8143,
                                    "var1": 266,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8138,
                                    "var1": 4,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8105,
                                    "var1": 8,
                                    "var2": 3,
                                    "var3": 0
                                }
                            ],
                            "style": 8100
                        },
                        {
                            "description": "subStyle",
                            "selections": [
                                {
                                    "perk": 8304,
                                    "var1": 12,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8347,
                                    "var1": 0,
                                    "var2": 0,
                                    "var3": 0
                                }
                            ],
                            "style": 8300
                        }
                    ]
                },
                "physicalDamageDealt": 179490,
                "physicalDamageDealtToChampions": 10198,
                "physicalDamageTaken": 20142,
                "profileIcon": 1228,
                "puuid": "B8Sil4R738tpoObqJUtZPITiLToQZNAZqbaLf6F37rwjAwJXf65l4qRhAVgD3vNwdUBm977UPciJ7Q",
                "quadraKills": 0,
                "riotIdName": "",
                "riotIdTagline": "",
                "role": "NONE",
                "sightWardsBoughtInGame": 0,
                "spell1Casts": 157,
                "spell2Casts": 89,
                "spell3Casts": 85,
                "spell4Casts": 10,
                "summoner1Casts": 4,
                "summoner1Id": 4,
                "summoner2Casts": 24,
                "summoner2Id": 11,
                "summonerId": "prC5XRbay3v0m4KOX3Yi1wnA1mtVCcoCS8FHgRFnagcjCdw",
                "summonerLevel": 137,
                "summonerName": "MeThudZ",
                "teamEarlySurrendered": false,
                "teamId": 200,
                "teamPosition": "JUNGLE",
                "timeCCingOthers": 9,
                "timePlayed": 2077,
                "totalDamageDealt": 204707,
                "totalDamageDealtToChampions": 11847,
                "totalDamageShieldedOnTeammates": 0,
                "totalDamageTaken": 37035,
                "totalHeal": 15249,
                "totalHealsOnTeammates": 0,
                "totalMinionsKilled": 33,
                "totalTimeCCDealt": 349,
                "totalTimeSpentDead": 218,
                "totalUnitsHealed": 1,
                "tripleKills": 0,
                "trueDamageDealt": 16386,
                "trueDamageDealtToChampions": 840,
                "trueDamageTaken": 970,
                "turretKills": 0,
                "turretTakedowns": 0,
                "turretsLost": 8,
                "unrealKills": 0,
                "visionScore": 26,
                "visionWardsBoughtInGame": 2,
                "wardsKilled": 2,
                "wardsPlaced": 11,
                "win": false
            },
            {
                "assists": 3,
                "baronKills": 0,
                "bountyLevel": 0,
                "champExperience": 18232,
                "champLevel": 17,
                "championId": 134,
                "championName": "Syndra",
                "championTransform": 0,
                "consumablesPurchased": 4,
                "damageDealtToBuildings": 4136,
                "damageDealtToObjectives": 6206,
                "damageDealtToTurrets": 4136,
                "damageSelfMitigated": 7267,
                "deaths": 5,
                "detectorWardsPlaced": 1,
                "doubleKills": 1,
                "dragonKills": 0,
                "firstBloodAssist": false,
                "firstBloodKill": false,
                "firstTowerAssist": false,
                "firstTowerKill": true,
                "gameEndedInEarlySurrender": false,
                "gameEndedInSurrender": true,
                "goldEarned": 15227,
                "goldSpent": 14500,
                "individualPosition": "MIDDLE",
                "inhibitorKills": 0,
                "inhibitorTakedowns": 0,
                "inhibitorsLost": 1,
                "item0": 6655,
                "item1": 3040,
                "item2": 3157,
                "item3": 1058,
                "item4": 3135,
                "item5": 3020,
                "item6": 3364,
                "itemsPurchased": 23,
                "killingSprees": 2,
                "kills": 9,
                "lane": "MIDDLE",
                "largestCriticalStrike": 0,
                "largestKillingSpree": 4,
                "largestMultiKill": 2,
                "longestTimeSpentLiving": 813,
                "magicDamageDealt": 192027,
                "magicDamageDealtToChampions": 38282,
                "magicDamageTaken": 12057,
                "neutralMinionsKilled": 8,
                "nexusKills": 0,
                "nexusLost": 0,
                "nexusTakedowns": 0,
                "objectivesStolen": 0,
                "objectivesStolenAssists": 0,
                "participantId": 8,
                "pentaKills": 0,
                "perks": {
                    "statPerks": {
                        "defense": 5003,
                        "flex": 5008,
                        "offense": 5005
                    },
                    "styles": [
                        {
                            "description": "primaryStyle",
                            "selections": [
                                {
                                    "perk": 8214,
                                    "var1": 2595,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8226,
                                    "var1": 250,
                                    "var2": 1352,
                                    "var3": 0
                                },
                                {
                                    "perk": 8210,
                                    "var1": 23,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8236,
                                    "var1": 48,
                                    "var2": 0,
                                    "var3": 0
                                }
                            ],
                            "style": 8200
                        },
                        {
                            "description": "subStyle",
                            "selections": [
                                {
                                    "perk": 8345,
                                    "var1": 3,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8347,
                                    "var1": 0,
                                    "var2": 0,
                                    "var3": 0
                                }
                            ],
                            "style": 8300
                        }
                    ]
                },
                "physicalDamageDealt": 11166,
                "physicalDamageDealtToChampions": 625,
                "physicalDamageTaken": 5242,
                "profileIcon": 4797,
                "puuid": "_ES7JGtYOHmERubPlHcHmgH0qk77AEpeg6IVlvHqxSL5uqyq885dujtDOl_-SMOHjdP78wfTa2msfw",
                "quadraKills": 0,
                "riotIdName": "",
                "riotIdTagline": "",
                "role": "SOLO",
                "sightWardsBoughtInGame": 0,
                "spell1Casts": 168,
                "spell2Casts": 131,
                "spell3Casts": 52,
                "spell4Casts": 11,
                "summoner1Casts": 3,
                "summoner1Id": 12,
                "summoner2Casts": 4,
                "summoner2Id": 4,
                "summonerId": "ADhwmlZoX8Tn7dVefpBy08FO9nEv6gcCplnXNBuneHBTvw8",
                "summonerLevel": 342,
                "summonerName": "Hold My Pock3t",
                "teamEarlySurrendered": false,
                "teamId": 200,
                "teamPosition": "MIDDLE",
                "timeCCingOthers": 56,
                "timePlayed": 2077,
                "totalDamageDealt": 204120,
                "totalDamageDealtToChampions": 38979,
                "totalDamageShieldedOnTeammates": 0,
                "totalDamageTaken": 18848,
                "totalHeal": 3650,
                "totalHealsOnTeammates": 0,
                "totalMinionsKilled": 264,
                "totalTimeCCDealt": 297,
                "totalTimeSpentDead": 189,
                "totalUnitsHealed": 1,
                "tripleKills": 0,
                "trueDamageDealt": 927,
                "trueDamageDealtToChampions": 72,
                "trueDamageTaken": 1548,
                "turretKills": 2,
                "turretTakedowns": 3,
                "turretsLost": 8,
                "unrealKills": 0,
                "visionScore": 14,
                "visionWardsBoughtInGame": 2,
                "wardsKilled": 1,
                "wardsPlaced": 4,
                "win": false
            },
            {
                "assists": 8,
                "baronKills": 0,
                "bountyLevel": 0,
                "champExperience": 14930,
                "champLevel": 16,
                "championId": 202,
                "championName": "Jhin",
                "championTransform": 0,
                "consumablesPurchased": 3,
                "damageDealtToBuildings": 1732,
                "damageDealtToObjectives": 12477,
                "damageDealtToTurrets": 1732,
                "damageSelfMitigated": 9069,
                "deaths": 6,
                "detectorWardsPlaced": 1,
                "doubleKills": 1,
                "dragonKills": 0,
                "firstBloodAssist": false,
                "firstBloodKill": true,
                "firstTowerAssist": false,
                "firstTowerKill": false,
                "gameEndedInEarlySurrender": false,
                "gameEndedInSurrender": true,
                "goldEarned": 14216,
                "goldSpent": 13200,
                "individualPosition": "BOTTOM",
                "inhibitorKills": 0,
                "inhibitorTakedowns": 0,
                "inhibitorsLost": 1,
                "item0": 6671,
                "item1": 3140,
                "item2": 6676,
                "item3": 3009,
                "item4": 3094,
                "item5": 3035,
                "item6": 3340,
                "itemsPurchased": 19,
                "killingSprees": 3,
                "kills": 8,
                "lane": "BOTTOM",
                "largestCriticalStrike": 1064,
                "largestKillingSpree": 3,
                "largestMultiKill": 2,
                "longestTimeSpentLiving": 671,
                "magicDamageDealt": 15347,
                "magicDamageDealtToChampions": 1424,
                "magicDamageTaken": 11331,
                "neutralMinionsKilled": 7,
                "nexusKills": 0,
                "nexusLost": 0,
                "nexusTakedowns": 0,
                "objectivesStolen": 0,
                "objectivesStolenAssists": 0,
                "participantId": 9,
                "pentaKills": 0,
                "perks": {
                    "statPerks": {
                        "defense": 5002,
                        "flex": 5008,
                        "offense": 5008
                    },
                    "styles": [
                        {
                            "description": "primaryStyle",
                            "selections": [
                                {
                                    "perk": 8021,
                                    "var1": 1926,
                                    "var2": 1612,
                                    "var3": 0
                                },
                                {
                                    "perk": 8009,
                                    "var1": 2042,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 9103,
                                    "var1": 20,
                                    "var2": 20,
                                    "var3": 0
                                },
                                {
                                    "perk": 8014,
                                    "var1": 283,
                                    "var2": 0,
                                    "var3": 0
                                }
                            ],
                            "style": 8000
                        },
                        {
                            "description": "subStyle",
                            "selections": [
                                {
                                    "perk": 8345,
                                    "var1": 3,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8352,
                                    "var1": 30,
                                    "var2": 126,
                                    "var3": 14
                                }
                            ],
                            "style": 8300
                        }
                    ]
                },
                "physicalDamageDealt": 177349,
                "physicalDamageDealtToChampions": 13380,
                "physicalDamageTaken": 12452,
                "profileIcon": 3184,
                "puuid": "Ulsl-a6cg7kMpJEVQ4Pz9s22-7GEsbbFnsIULLbSzNbQLwncP_dGGJTGb42_LymUhr4YXFIZkS8EJA",
                "quadraKills": 0,
                "riotIdName": "",
                "riotIdTagline": "",
                "role": "CARRY",
                "sightWardsBoughtInGame": 0,
                "spell1Casts": 84,
                "spell2Casts": 31,
                "spell3Casts": 33,
                "spell4Casts": 35,
                "summoner1Casts": 6,
                "summoner1Id": 7,
                "summoner2Casts": 4,
                "summoner2Id": 4,
                "summonerId": "zEj3ncrfqKy5xWG-Bxt33JVP0_VjhC73pIioPxz-Dnr-sMM",
                "summonerLevel": 72,
                "summonerName": "ImaJhìnation",
                "teamEarlySurrendered": false,
                "teamId": 200,
                "teamPosition": "BOTTOM",
                "timeCCingOthers": 37,
                "timePlayed": 2077,
                "totalDamageDealt": 193313,
                "totalDamageDealtToChampions": 15197,
                "totalDamageShieldedOnTeammates": 0,
                "totalDamageTaken": 24802,
                "totalHeal": 6277,
                "totalHealsOnTeammates": 1198,
                "totalMinionsKilled": 236,
                "totalTimeCCDealt": 193,
                "totalTimeSpentDead": 176,
                "totalUnitsHealed": 3,
                "tripleKills": 0,
                "trueDamageDealt": 616,
                "trueDamageDealtToChampions": 392,
                "trueDamageTaken": 1018,
                "turretKills": 0,
                "turretTakedowns": 1,
                "turretsLost": 8,
                "unrealKills": 0,
                "visionScore": 33,
                "visionWardsBoughtInGame": 2,
                "wardsKilled": 3,
                "wardsPlaced": 13,
                "win": false
            },
            {
                "assists": 11,
                "baronKills": 0,
                "bountyLevel": 0,
                "champExperience": 11598,
                "champLevel": 14,
                "championId": 235,
                "championName": "Senna",
                "championTransform": 0,
                "consumablesPurchased": 6,
                "damageDealtToBuildings": 643,
                "damageDealtToObjectives": 1191,
                "damageDealtToTurrets": 643,
                "damageSelfMitigated": 13447,
                "deaths": 11,
                "detectorWardsPlaced": 2,
                "doubleKills": 0,
                "dragonKills": 0,
                "firstBloodAssist": true,
                "firstBloodKill": false,
                "firstTowerAssist": false,
                "firstTowerKill": false,
                "gameEndedInEarlySurrender": false,
                "gameEndedInSurrender": true,
                "goldEarned": 8626,
                "goldSpent": 8150,
                "individualPosition": "UTILITY",
                "inhibitorKills": 0,
                "inhibitorTakedowns": 0,
                "inhibitorsLost": 1,
                "item0": 3864,
                "item1": 6632,
                "item2": 3111,
                "item3": 3094,
                "item4": 6677,
                "item5": 0,
                "item6": 3364,
                "itemsPurchased": 21,
                "killingSprees": 0,
                "kills": 1,
                "lane": "BOTTOM",
                "largestCriticalStrike": 291,
                "largestKillingSpree": 0,
                "largestMultiKill": 1,
                "longestTimeSpentLiving": 515,
                "magicDamageDealt": 2136,
                "magicDamageDealtToChampions": 973,
                "magicDamageTaken": 15321,
                "neutralMinionsKilled": 0,
                "nexusKills": 0,
                "nexusLost": 0,
                "nexusTakedowns": 0,
                "objectivesStolen": 0,
                "objectivesStolenAssists": 0,
                "participantId": 10,
                "pentaKills": 0,
                "perks": {
                    "statPerks": {
                        "defense": 5002,
                        "flex": 5008,
                        "offense": 5008
                    },
                    "styles": [
                        {
                            "description": "primaryStyle",
                            "selections": [
                                {
                                    "perk": 8351,
                                    "var1": 46,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8304,
                                    "var1": 9,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8321,
                                    "var1": 3,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 8410,
                                    "var1": 99,
                                    "var2": 0,
                                    "var3": 0
                                }
                            ],
                            "style": 8300
                        },
                        {
                            "description": "subStyle",
                            "selections": [
                                {
                                    "perk": 8009,
                                    "var1": 1626,
                                    "var2": 0,
                                    "var3": 0
                                },
                                {
                                    "perk": 9104,
                                    "var1": 21,
                                    "var2": 40,
                                    "var3": 0
                                }
                            ],
                            "style": 8000
                        }
                    ]
                },
                "physicalDamageDealt": 43533,
                "physicalDamageDealtToChampions": 17768,
                "physicalDamageTaken": 14512,
                "profileIcon": 5158,
                "puuid": "s3SXI23BnHU98aeyJil9_-tPk-NIB9Kojs981sTFOQC2udkc31jVOpGmPtoF7dD2_TXSrvdfXhj0XA",
                "quadraKills": 0,
                "riotIdName": "",
                "riotIdTagline": "",
                "role": "SUPPORT",
                "sightWardsBoughtInGame": 0,
                "spell1Casts": 60,
                "spell2Casts": 49,
                "spell3Casts": 19,
                "spell4Casts": 7,
                "summoner1Casts": 5,
                "summoner1Id": 14,
                "summoner2Casts": 5,
                "summoner2Id": 4,
                "summonerId": "hWC5TxtXggGXChKGswf0TWzABke6kVWkAX0aQibbV9jrkgU",
                "summonerLevel": 221,
                "summonerName": "Feras",
                "teamEarlySurrendered": false,
                "teamId": 200,
                "teamPosition": "UTILITY",
                "timeCCingOthers": 56,
                "timePlayed": 2077,
                "totalDamageDealt": 46223,
                "totalDamageDealtToChampions": 19294,
                "totalDamageShieldedOnTeammates": 1636,
                "totalDamageTaken": 31054,
                "totalHeal": 10834,
                "totalHealsOnTeammates": 1864,
                "totalMinionsKilled": 20,
                "totalTimeCCDealt": 162,
                "totalTimeSpentDead": 339,
                "totalUnitsHealed": 4,
                "tripleKills": 0,
                "trueDamageDealt": 553,
                "trueDamageDealtToChampions": 553,
                "trueDamageTaken": 1220,
                "turretKills": 1,
                "turretTakedowns": 1,
                "turretsLost": 8,
                "unrealKills": 0,
                "visionScore": 60,
                "visionWardsBoughtInGame": 2,
                "wardsKilled": 7,
                "wardsPlaced": 26,
                "win": false
            }
        ],
        "platformId": "NA1",
        "queueId": 420,
        "teams": [
            {
                "bans": [
                    {
                        "championId": 875,
                        "pickTurn": 1
                    },
                    {
                        "championId": 412,
                        "pickTurn": 2
                    },
                    {
                        "championId": 23,
                        "pickTurn": 3
                    },
                    {
                        "championId": 350,
                        "pickTurn": 4
                    },
                    {
                        "championId": 157,
                        "pickTurn": 5
                    }
                ],
                "objectives": {
                    "baron": {
                        "first": false,
                        "kills": 0
                    },
                    "champion": {
                        "first": false,
                        "kills": 41
                    },
                    "dragon": {
                        "first": false,
                        "kills": 3
                    },
                    "inhibitor": {
                        "first": true,
                        "kills": 1
                    },
                    "riftHerald": {
                        "first": false,
                        "kills": 0
                    },
                    "tower": {
                        "first": false,
                        "kills": 8
                    }
                },
                "teamId": 100,
                "win": true
            },
            {
                "bans": [
                    {
                        "championId": 166,
                        "pickTurn": 6
                    },
                    {
                        "championId": 517,
                        "pickTurn": 7
                    },
                    {
                        "championId": 23,
                        "pickTurn": 8
                    },
                    {
                        "championId": 68,
                        "pickTurn": 9
                    },
                    {
                        "championId": 104,
                        "pickTurn": 10
                    }
                ],
                "objectives": {
                    "baron": {
                        "first": false,
                        "kills": 0
                    },
                    "champion": {
                        "first": true,
                        "kills": 24
                    },
                    "dragon": {
                        "first": true,
                        "kills": 2
                    },
                    "inhibitor": {
                        "first": false,
                        "kills": 0
                    },
                    "riftHerald": {
                        "first": true,
                        "kills": 1
                    },
                    "tower": {
                        "first": true,
                        "kills": 3
                    }
                },
                "teamId": 200,
                "win": false
            }
        ],
        "tournamentCode": ""
    }
})
        
        tiltBot.run(true)
    })
})
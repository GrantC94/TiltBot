var botUtils, fs, fetch, botID, leagueKey, championKeys, numberOfSummoners, mostRecentGames, oldMostRecentGames, mostRecentLoss;

botUtils            = require('./botUtils.js');
fetch               = require('node-fetch');
botID               = process.env.BOT_ID;
leagueKey           = process.env.LEAGUE_KEY;
championKeys        = {};
mostRecentGames     = {};
oldMostRecentGames  = {};
mostRecentLoss      = [];

function initialize(enabled) {
  if(!enabled) {
    console.log("Tiltbot Disabled.")
  } else {
    console.log("Initiating Tiltbot")
    loadChampionInfo();
    run(true);
  }
}

function run(enabled) {
  if(enabled) {
    for (var summonerName in botUtils.leagueAccountInfo) {
      getGames(botUtils.leagueAccountInfo[summonerName].puuid, summonerName, "solo", 420)
      getGames(botUtils.leagueAccountInfo[summonerName].puuid, summonerName, "flex", 440)
      getGames(botUtils.leagueAccountInfo[summonerName].puuid, summonerName, "clash", 700)
    }
  }
}

function singleSummonerReload(summonerName) {
  summonerName = summonerName.replace(/ /g, "%20")
  if(botUtils.leagueAccountInfo[summonerName] != undefined) {
    getGames(botUtils.leagueAccountInfo[summonerName].puuid, summonerName, "solo", 420)
    getGames(botUtils.leagueAccountInfo[summonerName].puuid, summonerName, "flex", 440)
    getGames(botUtils.leagueAccountInfo[summonerName].puuid, summonerName, "clash", 700)
    return;
  }
  console.log('Invalid name passed to singleSummonerReload. Invalid Name: ' + summonerName);
}

function getGames(puuid, summonerName, queueName, queueCode) {
  var options, gameMapKey;

  options = {
    hostname: 'americas.api.riotgames.com',
    path: '/lol/match/v5/matches/by-puuid/' + puuid + '/ids?queue=' + queueCode + '&count=1&start=0&api_key=' + leagueKey,
    method: 'GET'
  };
  gameMapKey = summonerName + queueName;

  function getGamesOnResponse(res) {
    if(res.statusCode == 200) {
      res.on('data', function (chunk) {
        var gameId = JSON.parse(chunk)[0]
        if(mostRecentGames[gameMapKey] != gameId) {
          oldMostRecentGames[gameMapKey] = mostRecentGames[gameMapKey];
          mostRecentGames[gameMapKey] = gameId;

          if(oldMostRecentGames[gameMapKey] != undefined) {
            getMostRecentGame(gameId, puuid);
          } else {
            console.log('Skipping ' + summonerName + '. ' + mostRecentGames[gameMapKey] + ' is the first ' + queueName + ' game loaded.')
          }
        }
      });
    } else if(res.statusCode == 429) {
      botUtils.sleep(4000)
      getGames(puuid, summonerName, queueName, queueCode);
    } else {
      console.log(options.path)
      console.log('rejecting bad status code ' + res.statusCode);
      console.log('failure for ' + puuid + ", summoner name: " + summonerName)
    }
  }

  botUtils.botRequest(options, getGamesOnResponse);
}

function getMostRecentGame(message, puuid) {
  var options;
  
  options = {
    hostname: 'americas.api.riotgames.com',
    path: '/lol/match/v5/matches/' + message + '?api_key=' + leagueKey,
    method: 'GET'
  };

  function getMostRecentGamesOnRespone(res) {
    if(res.statusCode == 200) {
        var stats = [];
        var data = [];
        res.on('data', function (chunk) {
          data.push(chunk);
        });
        res.on('end', function() {
          var result = JSON.parse(data.join(''))
          for (var i = 0; i < result.info.participants.length; i++) {
              if(result.info.participants[i].puuid == puuid) {
                stats[0] = result.info.participants[i].summonerName
                stats[1] = result.info.participants[i].win
                stats[2] = result.info.participants[i].kills
                stats[3] = result.info.participants[i].deaths
                stats[4] = result.info.participants[i].assists
                stats[5] = championKeys[result.info.participants[i].championId]
              }
          }
          if(!stats[1]) {
            mostRecentLoss[stats[0]] = stats;
            tilt(stats)
          } else {
            console.log(stats[0] + ' Winned') 
          }
        });
      }  else if(res.statusCode == 429) {
        botUtils.sleep(4000)
        getMostRecentGame(message, puuid)
        console.log('rejecting bad status code ' + res.statusCode);
      }
  }

  botUtils.botRequest(options, getMostRecentGamesOnRespone);
}

function tilt(stats) {
  var tiltMessage, options, body;

  tiltMessage = 'Yikes! ' + stats[0] + ' just lost a League game! They went ' + stats[2] + '/' + stats[3] + '/' + stats[4] + ' on ' + stats[5] + '! That\'s a tilter!';

  options = {
    hostname: 'api.groupme.com',
    path: '/v3/bots/post',
    method: 'POST'
  };
  
  body = {
    "bot_id" : botID,
    "text" : tiltMessage
  };

  console.log('sending ' + tiltMessage + ' to ' + botID);

  function tiltOnResponse(res) {
    if(res.statusCode == 202) {
        console.log('sent ' + tiltMessage + ' to ' + botID);
      } else {
        console.log('failed sending ' + tiltMessage + ' to ' + botID);
        console.log('rejecting bad status code ' + res.statusCode);
      }
  }

  botUtils.botRequest(options, tiltOnResponse, body);
}

function loadChampionInfo() {
  let url = "http://ddragon.leagueoflegends.com/cdn/12.20.1/data/en_US/champion.json";

  let settings = { method: "Get" };

  fetch(url, settings)
    .then(res => res.json())
    .then((json) => {
        for (var champion in json.data) {
          championKeys[json.data[champion].key] = champion
        }
    });
    console.log(championKeys);
}

function getMostRecentGames() {
  return mostRecentGames;
}

async function tiltCheck(summonerName) {
  await botUtils.sendGroupmeMessage("Let me check the back...")
  botUtils.sleep(120000)
  
  var theirMostRecentLoss = mostRecentLoss[summonerName]
  if(theirMostRecentLoss == undefined) {
    botUtils.sendGroupmeMessage("Yeah, looks like they haven't lost a game since I was restarted. Bummer.")
    return;
  }
  
  botUtils.sendGroupmeMessage('Ok I found a game where ' + theirMostRecentLoss[0] + ' lost on '
      + theirMostRecentLoss[5] + '. They went ' + theirMostRecentLoss[2] + '/' + theirMostRecentLoss[3] + '/' + theirMostRecentLoss[4] + '. That\'s pretty bad!');
}

exports.initialize = initialize;
exports.run = run;
exports.getMostRecentGames = getMostRecentGames;
exports.tiltCheck = tiltCheck;
exports.singleSummonerReload = singleSummonerReload;
var http, https,director, botUtils, tiltBot, 
tfTilter, botID, tfTilterEnable, tiltBotEnable, router, server, port;

http            = require('http');
https           = require('https');
director        = require('director');
botUtils        = require('./botUtils');
tiltBot         = require('./tiltBot.js');
tfTilter        = require('./tfTilter.js');
botID           = process.env.BOT_ID;
tfTilterEnable  = process.env.TFTILTER_ENABLE == 'true'
tiltBotEnable   = process.env.TILTBOT_ENABLE == 'true'

//Set our API Routes up
router = new director.http.Router({
  '/' : {
    post: commandRouter,
    get: ping
  },
  '/currentGames' : {
    get: currentGamesList
  },
  '/tft/singleSummonerCheck' : {
    post: singleSummonerReloadTfTilter
  },
  '/lol/singleSummonerCheck' : {
    post: singleSummonerReloadTiltbot
  }
});

initializeBots();

//Server setup for handling http requests
server = http.createServer(function (req, res) {
  req.chunks = [];
  req.on('data', function (chunk) {
    req.chunks.push(chunk.toString());
  });

  router.dispatch(req, res, function(err) {
    res.writeHead(err.status, {"Content-Type": "text/plain"});
    res.end(err.message);
  });
});

port = Number(process.env.PORT || 5000);
server.listen(port);

//These pings keep the Heroku app awake
https.get("https://tiltbot2.herokuapp.com/");
setInterval(function() {
    https.get("https://tiltbot2.herokuapp.com/");
}, 300000);

//This is for periodic game checks
setInterval(function() {
  tiltBot.run(tiltBotEnable)
}, 600000)

setInterval(function() {
  tfTilter.run(tfTilterEnable)
}, 600000)

//Gets the bots started
async function initializeBots() {
  await botUtils.loadSummonerCache();
  tiltBot.initialize(tiltBotEnable);
  tfTilter.initialize(tfTilterEnable);
  return;
}

//Functions for endpoints
function ping() {
  this.res.writeHead(200);
  this.res.end("Hey, I'm Cool Guy.");
}

function currentGamesList() {
  console.log("Serving current games list");
  this.res.writeHead(200);
  this.res.end(JSON.stringify(tiltBot.getMostRecentGames()));
}

function singleSummonerReloadTfTilter() {
  console.log("Performing a single summoner reload for TfTiler");
  if(this.req.body != undefined && this.req.body.summonerName != undefined) {
    tfTilter.singleSummonerReload(this.req.body.summonerName)
    this.res.writeHead(204);
    this.res.end();
  } else {
    console.log("Nevermind, this dude can't write a good request");
    this.res.writeHead(400);
    this.res.end("Please include a summoner name in the body of your request");
  }
}

function singleSummonerReloadTiltbot() {
  console.log("Performing a single summoner reload for TiltBot");
  if(this.req.body != undefined && this.req.body.summonerName != undefined) {
    tiltBot.singleSummonerReload(this.req.body.summonerName)
    this.res.writeHead(204);
    this.res.end();
  } else {
    console.log("Nevermind, this dude can't write a good request");
    this.res.writeHead(400);
    this.res.end("Please include a summoner name in the body of your request");
  }
}

//Routes commands where they need to go
function commandRouter() {
  const command =  this.req.body.text.split(" ");
  if(command[0] == "!tiltcheck") {
    if(command.length > 2) {
      if(command[1].toLowerCase() == "tft") {
        tfTilter.tiltCheck(command.slice(2).join(' '));
      } else if(command[1].toLowerCase() == "lol") {
        tiltBot.tiltCheck(command.slice(2).join(' '));
      } else {
        errorResponse("It's '!tiltcheck <tft OR lol> <SummonerName>'. It's not hard, moron.");
      }
    } else {
       errorResponse("It's '!tiltcheck <tft OR lol> <SummonerName>'. It's not hard, moron.");
     }
  }
  
  this.res.writeHead(200);
  this.res.end();
}

//Handle Command Errors
function errorResponse(tiltMessage) {
  var options, body;


  options = {
    hostname: 'api.groupme.com',
    path: '/v3/bots/post',
    method: 'POST'
  };
  
  body = {
    "bot_id" : botID,
    "text" : tiltMessage
  };

  console.log('sending ' + tiltMessage + ' to ' + botID);


  console.log(options)
  console.log(body)
  function tiltOnResponse(res) {
    if(res.statusCode == 202) {
        console.log('success');
      } else {
        console.log('failed sending ' + tiltMessage + ' to ' + botID);
        console.log('rejecting bad status code ' + res.statusCode);
      }
  }

  botUtils.botRequest(options, tiltOnResponse, body);
}
